﻿using System;
using System.Collections;
using System.Net;
using System.Threading.Tasks;
using Mirror;
using RestSharp;
using UnityEngine;
using UnityEngine.Rendering;

public class ServerRegister : MonoBehaviour
{
    public string RegistryServerAdress = "http://94.19.120.243:5000";
    public string ServerName = "SomeDefaultServer";
    public TelepathyTransport telepathy;
    public Ignorance ignorance;

    private string ETAG;
    private int ID;
    private bool REGISTERED = false;

    private Reply reply;

    
    Guid _GUID = Guid.NewGuid();

    private void LogMessage(string title, string message)
    {
        Debug.Log(title + " " + message);
    }

    public static bool isHeadless => SystemInfo.graphicsDeviceType == GraphicsDeviceType.Null;

    // Start is called before the first frame update
    public static bool IsHeadless()
    {
        return isHeadless;
    }

    public int GetPort()
    {

        if (telepathy != null)
        {
            return telepathy.port;
        }

        if (ignorance != null)
        {
            return ignorance.CommunicationPort;
        }

        return 00;

    }

    public string GetPortType()
    {

        if (telepathy != null)
        {
            return "TCP";
        }

        if (ignorance != null)
        {
            return "UDP";
        }

        return "UNKNOWN";

    }

    public string GetMyIp()
    {

        string myip = new WebClient().DownloadString("http://icanhazip.com");
        myip = myip.Replace("\n", String.Empty);
        return myip;
    }

    void Start()
    {
        
        if (isHeadless)
        {
            ServerRegistration();


        }
    }

    [Serializable]
    public class ServerInfo
    {
        public string server_name { get; set; }
        public string ip { get; set; }
        public int port { get; set; }
        public string port_type { get; set; }
        public string UID { get; set; }
        public string timestamp { get; set; }

        public override string ToString()
        {
            return UnityEngine.JsonUtility.ToJson(this, true);
        }
    }

    [Serializable]
    public class Reply
    {

        public string _updated { get; set; }
        public string _created { get; set; }
        public string _etag  { get; set; }
        public int id  { get; set; }
        public string _status  { get; set; }
    }

    [Serializable]
    public class UpdateBody
    {
        public string timestamp;
    }


    
    void FixedUpdate()
    {
        if (!isRunning) StartCoroutine(ServerUpdate());
    }

    
    
    bool isRunning = false;
    IEnumerator ServerUpdate(){
        for(;;)
        {
            isRunning = true;
            yield return new WaitForSeconds(3*60);
            RegistryUpdate();
            isRunning = false;
        }
    }


    async Task ServerRegistration()
    {
        ServerInfo body = new ServerInfo
        {
            server_name = ServerName,
            ip = GetMyIp(),
            port = GetPort(),
            port_type = GetPortType(),
            UID = Convert.ToString(_GUID),
            timestamp = Convert.ToString(Time.deltaTime * 100)
        };

        var client = new RestClient(RegistryServerAdress);
        // client.Authenticator = new HttpBasicAuthenticator(username, password);

        var request = new RestRequest("/servers",Method.POST);
        request.AddJsonBody(body);
        // easily add HTTP Headers
        request.AddHeader("content-type", "application/json");
        
        // async with deserialization
        client.ExecuteAsync<Reply>(request, response => {
            if (response.Data._status == "OK")
            {
                reply = response.Data;
                REGISTERED = true;
                ETAG = response.Data._etag;
                ID = response.Data.id;
                Debug.Log("Registration OK ETAG :" + ETAG + " ID:" + ID);
            }
        });

    }

    void RegistryUpdate()
    {
        UpdateBody body = new UpdateBody
        {
            timestamp = Convert.ToString(Time.deltaTime * 100)
        };
        
        var client = new RestClient(RegistryServerAdress);
        // client.Authenticator = new HttpBasicAuthenticator(username, password);

        var request = new RestRequest("/servers/"+ID,Method.PATCH);
        request.AddJsonBody(body);
        // easily add HTTP Headers
        request.AddHeader("content-type", "application/json");
        request.AddHeader("if-match", ETAG);

        // async with deserialization
        client.ExecuteAsync<Reply>(request, response => {
            if (response.Data._status == "OK")
            {
                reply = response.Data;
                ETAG = response.Data._etag;
                ID = response.Data.id;
                print("Status Update: OK"+"ETAG: "+ETAG);
            }
        });
        
    }
}
