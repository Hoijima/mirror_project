//using Unity.Collections;
//using Unity.Collections.LowLevel.Unsafe;
//using Unity.Jobs;
//using Unity.Burst;
//using Unity.Collections;
//using Unity.Entities;
//using Unity.Jobs;
//using UnityEngine;
//
//public class SpawnSystem : JobComponentSystem
//{
//    private EndFrameBarrier _endFrameBarrier;
//
//    [BurstCompile]
//    struct SpawnJob : IJobProcessComponentData<SpawnRequestData>
//    {
//        [WriteOnly] public EntityCommandBuffer.Concurrent Buffer;
//
//        [ReadOnly] public NativeArray<Entity> Prefabs;
//
//        [ReadOnly] public NativeMultiHashMap<int, int> PrefabComponents;
//
//        [NativeSetThreadIndex] private int _threadIndex;
//
//        public void Execute(ref SpawnRequestData data)
//        {
//            int tagIndex = data.EntityTagTypeIndex;
//
//            for (int i = 0; i < Prefabs.Length; i++)
//            {
//                // Check if type of the component is the same as the type of the request component type
//                // Equals to if the entity has component of type
//
//                if (!PrefabComponents.TryGetFirstValue(i, out int firstIndex, out var iterator)) continue;
//
//                // Complete condition
//                if (CompareIndex(firstIndex, tagIndex, i)) return;
//
//                while (PrefabComponents.TryGetNextValue(out int typeIndex, ref iterator))
//                {
//                    // Complete condition
//                    if (CompareIndex(typeIndex, tagIndex, i)) return;
//                }
//            }
//        }
//
//        private bool CompareIndex(int typeIndex, int tagIndex, int prefabIndex)
//        {
//            if (typeIndex != tagIndex) return false;
//
//            Buffer.Instantiate(_threadIndex, Prefabs[prefabIndex]);
//            return true;
//        }
//    }
//
//    protected override JobHandle OnUpdate(JobHandle inputDeps) {
//        SpawnJob spawnJob = new SpawnJob {
//            Prefabs = SpawnSystemExt.Prefabs,
//            PrefabComponents = SpawnSystemExt.PrefabComponents,
//            Buffer = _endFrameBarrier.CreateCommandBuffer().ToConcurrent()
//        };
//        JobHandle handle = spawnJob.Schedule(this, inputDeps);
//        _endFrameBarrier.AddJobHandleForProducer(handle);
// 
//        return handle;
//    }
//}
//
//internal struct SpawnRequestData : IComponentData
//{
//}