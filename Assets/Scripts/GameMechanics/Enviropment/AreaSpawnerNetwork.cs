//
// Copyright (c) Brian Hernandez. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root for details.
//

using Mirror;
using UnityEngine;
using Random = UnityEngine.Random;

public enum AreaSpawnerShape
{
    Box,
    Sphere,
}

// Used mostly for testing to provide stuff to fly around and into.
public class AreaSpawnerNetwork : NetworkBehaviour
{
    [Header("General settings:")]

    [Tooltip("Prefab to spawn.")]
    public Transform prefab;

    [Tooltip("Shape to spawn the prefabs in.")]
    public AreaSpawnerShape spawnShape = AreaSpawnerShape.Sphere;

    [Tooltip("Multiplier for the spawn shape in each axis.")]
    public Vector3 shapeModifiers = Vector3.one;

    [Tooltip("How many prefab to spawn.")]
    public int asteroidCount = 50;

    [Tooltip("Distance from the center of the gameobject that prefabs will spawn")]
    public float range = 1000.0f;

    [Tooltip("Should prefab have a random rotation applied to it.")]
    public bool randomRotation = true;

    [Tooltip("Random min/max scale to apply.")]
    public Vector2 scaleRange = new Vector2(1.0f, 3.0f);

    [Header("Rigidbody settings:")]

    [Tooltip("Apply a velocity from 0 to this value in a random direction.")]
    public float velocity = 0.0f;

    [Tooltip("Apply an angular velocity (deg/s) from 0 to this value in a random direction.")]
    public float angularVelocity = 0.0f;

    [Tooltip("If true, raise the mass of the object based on its scale.")]
    public bool scaleMass = true;

    private Vector3 spawnPos;
    
    // Use this for initialization
    void Start()
    {
        if (prefab != null)
        {
            for (int i = 0; i < asteroidCount; i++)
                CreateAsteroid();
        }
    }

    private void CreateAsteroid()
    {
        spawnPos = Vector3.zero;
         
        // Create random position based on specified shape and range.
        if (spawnShape == AreaSpawnerShape.Box)
        {
            spawnPos.x = Random.Range(-range, range) * shapeModifiers.x;
            spawnPos.y = Random.Range(-range, range) * shapeModifiers.y;
            spawnPos.z = Random.Range(-range, range) * shapeModifiers.z;
        }
        else if (spawnShape == AreaSpawnerShape.Sphere)
        {
            spawnPos = Random.insideUnitSphere * range;
            spawnPos.x *= shapeModifiers.x;
            spawnPos.y *= shapeModifiers.y;
            spawnPos.z *= shapeModifiers.z;
        }

        // Offset position to match position of the parent gameobject.
        spawnPos += transform.position;
        
        CmdSpawn();


    }

    [Command]
    void CmdSpawn()
    {
        
        Quaternion spawnRot = (randomRotation) ? Random.rotation : Quaternion.identity;

        
        var roid = Instantiate(prefab, spawnPos, spawnRot);
        roid.SetParent(transform);
        // Apply scaling.
        float scale = Random.Range(scaleRange.x, scaleRange.y);
        roid.localScale = Vector3.one * scale;

        // Apply rigidbody values.
        Rigidbody r = prefab.GetComponent<Rigidbody>();
        if (r)
        {
            if (scaleMass)
                r.mass *= scale * scale * scale;

            r.AddRelativeForce(Random.insideUnitSphere * velocity, ForceMode.VelocityChange);
            r.AddRelativeTorque(Random.insideUnitSphere * angularVelocity * Mathf.Deg2Rad, ForceMode.VelocityChange);
        }

        NetworkServer.Spawn(roid.gameObject);
    }
    
    public void CreateNewAstroid()
    {
        CreateAsteroid();
    }


}


